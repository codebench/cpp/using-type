#include <iostream>
class B{
    public: using type=std::string;
};
class D : public B{
  //  public: using type=int;
};
int main(){
  //     typename B::type str="abc";
     typename D::type str="xyz";

    std::cout << "str = " << str << std::endl;
}
